package com.otp.verify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootOtpGmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootOtpGmailApplication.class, args);
	}

}
