package com.otp.verify.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.otp.verify.dto.LoginDto;
import com.otp.verify.dto.RegisterDto;
import com.otp.verify.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	UserService userService;
	
	@PostMapping("/register")
	public ResponseEntity<String> register(@RequestBody RegisterDto registerDto){
		return new ResponseEntity<>(this.userService.register(registerDto),HttpStatus.OK);
	}
	
	@PutMapping("/verify")
	public ResponseEntity<String> verifyEmail(@RequestParam String email,@RequestParam String otp){
		return new ResponseEntity<>(this.userService.verifyAccount(email,otp),HttpStatus.OK);
	}
	
	@PutMapping("/regenerate-otp")
	  public ResponseEntity<String> regenerateOtp(@RequestParam String email) {
	    return new ResponseEntity<>(userService.regenerateOtp(email), HttpStatus.OK);
	  }
	 @PutMapping("/login")
	  public ResponseEntity<String> login(@RequestBody LoginDto loginDto) {
	    return new ResponseEntity<>(userService.login(loginDto), HttpStatus.OK);
	  }
	

}
