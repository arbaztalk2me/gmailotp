package com.otp.verify.service;

import java.time.Duration;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.otp.verify.dto.LoginDto;
import com.otp.verify.dto.RegisterDto;
import com.otp.verify.entity.User;
import com.otp.verify.repository.UserRepo;
import com.otp.verify.util.EmailUtil;
import com.otp.verify.util.OtpUtil;

import jakarta.mail.MessagingException;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepo userRepo;

	@Autowired
	OtpUtil otpUtil;

	@Autowired
	EmailUtil emailUtil;

	@Override
	public String register(RegisterDto registerDto) {

		String otp = otpUtil.generateOtp();
		try {
			emailUtil.sendOtpEmail(registerDto.getEmail(), otp);
		} catch (MessagingException e) {
			throw new RuntimeException("Unable to send otp please try again");
		}
		User user = new User();
	    user.setName(registerDto.getName());
	    user.setEmail(registerDto.getEmail());
	    user.setPassword(registerDto.getPassword());
	    user.setOtp(otp);
	    user.setOtpGeneratedTime(LocalDateTime.now());
	    userRepo.save(user);
	    return "User registration successful";
	}

	@Override
	public String verifyAccount(String email, String otp) {
		User u=this.userRepo.findByEmail(email).orElseThrow(() -> new RuntimeException("User not found with this email: " + email));
		if(u.getOtp().equals(otp)&&Duration.between(u.getOtpGeneratedTime(), LocalDateTime.now()).getSeconds()<(1*60)) {
			u.setActive(true);
			this.userRepo.save(u);
			return "OTP verified you can login";
		}
		return "Please regenerate otp and try again";
	}

	@Override
	public String regenerateOtp(String email) {
		User u=this.userRepo.findByEmail(email).orElseThrow(() -> new RuntimeException("User not found with this email: " + email));
		String generateOtp = this.otpUtil.generateOtp();
		try {
		      emailUtil.sendOtpEmail(email, generateOtp);
		    } catch (MessagingException e) {
		      throw new RuntimeException("Unable to send otp please try again");
		    }
		u.setOtp(generateOtp);
		u.setOtpGeneratedTime(LocalDateTime.now());
		this.userRepo.save(u);
	    return "Email sent... please verify account within 1 minute";
	}

	@Override
	public String login(LoginDto loginDto) {
		User user = this.userRepo.findByEmail(loginDto.getEmail())
		        .orElseThrow(
		            () -> new RuntimeException("User not found with this email: " + loginDto.getEmail()));
		    if (!loginDto.getPassword().equals(user.getPassword())) {
		      return "Password is incorrect";
		    } else if (!user.isActive()) {
		      return "your account is not verified";
		    }
		    return "Login successful";
		  }
	
	
	

}
