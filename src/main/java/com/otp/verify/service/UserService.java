package com.otp.verify.service;

import com.otp.verify.dto.LoginDto;
import com.otp.verify.dto.RegisterDto;

public interface UserService {

	String register(RegisterDto registerDto);

	String verifyAccount(String email, String otp);

	String regenerateOtp(String email);

	String login(LoginDto loginDto);
	
}
